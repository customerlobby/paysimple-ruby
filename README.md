# Ruby client library for Paysimple API v4.0

Ruby client to interact with [PaySimple] API. Detailed API documentation can be found here: [documentation]

## Installation

Execute this command:

```ruby
gem install 'paysimple-ruby'
```

If you're using Rails, simply add this to your `Gemfile` :

```ruby
gem 'paysimple-ruby'
```

## Configuration

Initialize Paysimple client with user id and access token:

```ruby
require 'paysimple'
Paysimple.api_user = '<your api username>'
Paysimple.api_key = '<your api key>'
```

By default the library will try to connect to a 'live' mode that is production endpoint. You can customize this behavior by specifying API mode explicitly that is 'test'.

```ruby
Paysimple.mode = 'live'
```
This will make library to connect to production endpoint.

or

```ruby
Paysimple.mode = 'test'
```
This will make library to connect to sandbox endpoint.

## Usage examples

### Customer

#### Create new customer

```ruby
customer = Paysimple::Customer.create(
    first_name: 'John',
    last_name: 'Doe'
)
```
#### Update the customer
```ruby
updated_customer = Paysimple::Customer.update(
    id: customer[:id],
    first_name: 'John',
    last_name: 'Smith'
)
```
#### Get customer by ID
```ruby
customer = Paysimple::Customer.get(customer[:id])
```
#### Find customers
```ruby
customers = Paysimple::Customer.find(
    page: 1,
    page_size: 10,
    company: 'ABC company'
)
```
#### Get customer's payments and payment schedules
```ruby
payments = Paysimple::Customer.payments(
    customer[:id],
    { page_size: 5 }
)
payment_schedules = Paysimple::Customer.payment_schedules(
    customer[:id],
    { page_size: 5 }
)
```
#### Delete customer
```ruby
Paysimple::Customer.delete(customer[:id])
```

### CreditCard

#### Create credit card
```ruby
credit_card = Paysimple::CreditCard.create(
    customer_id: customer[:id],
    credit_card_number: '4111111111111111',
    expiration_date: '12/2021',
    billing_zip_code: '80202',
    issuer: Paysimple::Issuer::VISA,
    is_default: true
)
```
#### Update credit card
```ruby
updated_credit_card = Paysimple::CreditCard.update(
    id: credit_card[:id], ...
)
```
#### Delete credit card
```ruby
Paysimple::CreditCard.delete(credit_card[:id])
```


### ACH

#### Create ACH
```ruby
ach = Paysimple::ACH.create(
    customer_id: customer[:id],
    account_number: '751111111',
    routing_number: '131111114',
    bank_name: 'PaySimple Bank',
    is_checking_account: true,
    is_default: true
)
```

#### Update ACH
```ruby
updated_ach = Paysimple::ACH.update({ id: ach[:id], ... })
```

#### Delete ACH
```ruby
Paysimple::ACH.delete(ach[:id])
```

### Payment

#### Create payment
```ruby
payment = Paysimple::Payment.create(
    amount: 100,
    account_id: credit_card[:id]
)
```
#### Get payment
```ruby
payment = Paysimple::Payment.get(payment[:id])
```
#### Void payment
```ruby
Paysimple::Payment.void(payment[:id])
```
#### Refund payment
```ruby
Paysimple::Payment.refund(payment[:id])
```
#### Find payments
```ruby
payments = Paysimple::Payment.find(
    status: 'authorized',
    page_size: 10
)
```

### RecurringPayment

#### Create recurring payment
```ruby
recurring_payment = Paysimple::RecurringPayment.create(
    account_id: credit_card[:id],
    payment_amount: 10,
    start_date: Date.today,
    execution_frequency_type: Paysimple::ExecutionFrequencyType::DAILY
)
```
#### Get recurring payment
```ruby
recurring_payment = Paysimple::RecurringPayment.get(recurring_payment[:id])
```
#### Suspend recurring payment
```ruby
Paysimple::RecurringPayment.suspend(recurring_payment[:id])
```
#### Resume recurring payment
```ruby
Paysimple::RecurringPayment.resume(recurring_payment[:id])
```
#### Get payments connected with recurring payment
```ruby
payments = Paysimple::RecurringPayment.payments(recurring_payment[:id])
```
#### Find recurring payments
```ruby
recurring_payments = Paysimple::RecurringPayment.find(page_size: 10)
```
#### Delete recurring payment
```ruby
Paysimple::RecurringPayment.delete(payment[:id])
```

### PaymentPlan

#### Create payment plan

```ruby
payment_plan = Paysimple::PaymentPlan.create(
    account_id: credit_card[:id],
    total_due_amount: 10,
    total_number_of_payments: 3,
    start_date: Date.today,
    execution_frequency_type: Paysimple::ExecutionFrequencyType::DAILY
)
```
#### Get payment plan
```ruby
payment_plan = Paysimple::PaymentPlan.get(payment_plan[:id])
```
#### Suspend payment plan
```ruby
Paysimple::PaymentPlan.suspend(payment_plan[:id])
```
#### Resume payment plan
```ruby
Paysimple::PaymentPlan.resume(payment_plan[:id])
```
#### Get payments connected with payment plan
```ruby
payments = Paysimple::PaymentPlan.payments(payment_plan[:id])
```
#### Find payment plan
```ruby
payment_plans = Paysimple::PaymentPlan.find(page_size: 10)
```
#### Delete payment plan
```ruby
Paysimple::PaymentPlan.delete(payment_plan[:id])
```

[PaySimple]: http://www.paysimple.com
[documentation]: http://developer.paysimple.com/documentation/

