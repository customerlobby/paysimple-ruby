# frozen_string_literal: true

require 'spec_helper'

describe 'Paysimple' do
  # before(:each) do
  #   Paysimple.api_endpoint = Paysimple::Endpoint::SANDBOX
  #   Paysimple.api_user = ENV['API_USER']
  #   Paysimple.api_key = ENV['API_KEY']
  # end

  context 'customer' do
    it 'should create customer' do
      VCR.use_cassette('customers_create') do
        customer = Paysimple::Customer.create(first_name: 'John', last_name: 'Doe')[:response]
        expect(customer[:id]).not_to be_nil
        expect(customer[:first_name]).to eq('John')
        expect(customer[:last_name]).to eq('Doe')
      end
    end

    it 'should update customer' do
      VCR.use_cassette('customers_update') do
        customer = Paysimple::Customer.create(first_name: 'John', last_name: 'Doe')[:response]
        updated_customer = Paysimple::Customer.update(id: customer[:id], first_name: 'John', last_name: 'Smith')[:response]
        expect(updated_customer[:last_name]).to eq('Smith')
      end
    end

    it 'should delete customer' do
      VCR.use_cassette('customers_delete') do
        customer = Paysimple::Customer.create(first_name: 'John', last_name: 'Doe')[:response]
        expect { Paysimple::Customer.delete(customer[:id])[:response] }.to_not raise_error
      end
    end
  end

  context 'credit_card' do
    it 'should get credit card' do
      VCR.use_cassette('credit_card') do
        customer = Paysimple::Customer.create(first_name: 'John', last_name: 'Doe')[:response]
        credit_card = Paysimple::CreditCard.create(
          customer_id: customer[:id],
          credit_card_number: '4111111111111111',
          expiration_date: '12/2021',
          billing_zip_code: '80202',
          issuer: Paysimple::Issuer::VISA,
          is_default: true
        )[:response]

        expect(credit_card[:id]).not_to be_nil
        expect(Paysimple::CreditCard.get(credit_card[:id])[:response]).not_to be_nil
      end
    end

    it 'should create credit card, collect payment and void it' do
      VCR.use_cassette('create_credit_card') do
        customer = Paysimple::Customer.create(first_name: 'John', last_name: 'Doe')[:response]
        credit_card = Paysimple::CreditCard.create(
          customer_id: customer[:id],
          credit_card_number: '4111111111111111',
          expiration_date: '12/2021',
          billing_zip_code: '80202',
          issuer: Paysimple::Issuer::VISA,
          is_default: true
        )[:response]
        expect(credit_card[:id]).not_to be_nil
        payment = Paysimple::Payment.create(amount: 100, account_id: credit_card[:id])[:response]
        expect(payment[:status]).to eq('Authorized')
        void_result = Paysimple::Payment.void(payment[:id])[:response]
        expect(void_result[:status]).to eq('Voided')
      end
    end

    it 'should return payments' do
      VCR.use_cassette('payments') do
        payments = Paysimple::Payment.find(page: 1, page_size: 2, lite: false)[:response]
        expect(payments).to be_instance_of(Array)
        expect(payments.size).to be <= 2
      end
    end

    it 'should delete credit card' do
      VCR.use_cassette('delete_credit_card') do
        customer = Paysimple::Customer.create(first_name: 'John', last_name: 'Doe')[:response]
        credit_card = Paysimple::CreditCard.create(
          customer_id: customer[:id],
          credit_card_number: '4111111111111111',
          expiration_date: '12/2021',
          issuer: Paysimple::Issuer::VISA,
          is_default: true
        )[:response]
        expect { Paysimple::CreditCard.delete(credit_card[:id])[:response] }.to_not raise_error
      end
    end
  end

  context 'ACH account' do
    it 'should create ACH account' do
      VCR.use_cassette('ach_account_update') do
        customer = Paysimple::Customer.create(first_name: 'John', last_name: 'Doe')[:response]

        ach = Paysimple::ACH.create(
          customer_id: customer[:id],
          account_number: '751111111',
          routing_number: '131111114',
          bank_name: 'PaySimple Bank',
          is_checking_account: true,
          is_default: true
        )[:response]

        expect(ach[:id]).not_to be_nil
      end
    end

    it 'should delete ACH account' do
      VCR.use_cassette('ach_account_delete') do
        customer = Paysimple::Customer.create(first_name: 'John', last_name: 'Doe')[:response]

        ach = Paysimple::ACH.create(
          customer_id: customer[:id],
          account_number: '751111111',
          routing_number: '131111114',
          bank_name: 'PaySimple Bank',
          is_checking_account: true,
          is_default: true
        )[:response]

        expect { Paysimple::ACH.delete(ach[:id])[:response] }.to_not raise_error
      end
    end
  end

  context 'payment' do
    it 'should work with recurring payment' do
      VCR.use_cassette('recurring_payment') do
        customer = Paysimple::Customer.create(first_name: 'John', last_name: 'Doe')[:response]

        credit_card = Paysimple::CreditCard.create(
          customer_id: customer[:id],
          credit_card_number: '4111111111111111',
          expiration_date: '12/2021',
          billing_zip_code: '80202',
          issuer: Paysimple::Issuer::VISA,
          is_default: true
        )[:response]

        payment = Paysimple::RecurringPayment.create(
          account_id: credit_card[:id],
          payment_amount: 10,
          start_date: Date.today + 1,
          first_payment_date: Date.today,
          first_payment_amount: 100,
          execution_frequency_type: Paysimple::ExecutionFrequencyType::FIRST_OF_MONTH
        )[:response]

        expect(payment[:id]).not_to be_nil
        expect { Paysimple::RecurringPayment.suspend(payment[:id])[:response] }.to_not raise_error
        expect { Paysimple::RecurringPayment.resume(payment[:id])[:response] }.to_not raise_error
        payments = Paysimple::RecurringPayment.payments(payment[:id], {})[:response]
        expect(payments).to be_instance_of(Array)
      end
    end

    it 'should work with payment plans' do
      VCR.use_cassette('payment_plans') do
        customer = Paysimple::Customer.create(first_name: 'John', last_name: 'Doe')[:response]

        credit_card = Paysimple::CreditCard.create(
          customer_id: customer[:id],
          credit_card_number: '4111111111111111',
          expiration_date: '12/2021',
          billing_zip_code: '80202',
          issuer: Paysimple::Issuer::VISA,
          is_default: true
        )[:response]

        payment_plan = Paysimple::PaymentPlan.create(
          account_id: credit_card[:id],
          total_due_amount: 10,
          total_number_of_payments: 3,
          start_date: Date.today,
          execution_frequency_type: Paysimple::ExecutionFrequencyType::DAILY
        )[:response]

        expect(payment_plan[:id]).not_to be_nil
        expect { Paysimple::PaymentPlan.suspend(payment_plan[:id])[:response] }.to_not raise_error
        expect { Paysimple::PaymentPlan.resume(payment_plan[:id])[:response] }.to_not raise_error
        payments = Paysimple::PaymentPlan.payments(payment_plan[:id])[:response]
        expect(payments).to be_instance_of(Array)
      end
    end
  end

  context 'credentials' do
    it 'should raise if API key is not provided' do
      VCR.use_cassette('api_key_absent') do
        Paysimple.api_key = ''
        expect { Paysimple::Customer.create(first_name: 'John')[:response] }.to raise_error(Paysimple::AuthenticationError)
      end
    end

    it 'should raise if not all required parameters are provided' do
      VCR.use_cassette('invalid_request') do
        expect { Paysimple::Customer.create(first_name: 'John')[:response] }.to raise_error(Paysimple::AuthenticationError)
      end
    end

    it 'should raise if API key is invalid' do
      VCR.use_cassette('invalid_api_key') do
        Paysimple.api_key = '<invalid key>'
        expect do
          Paysimple::Customer.create(first_name: 'John', last_name: 'Doe')[:response]
        end.to raise_error(Paysimple::AuthenticationError)
      end
    end
  end
end
