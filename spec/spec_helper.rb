# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'paysimple'
require 'vcr'

RSpec.configure do |config|
  config.color = true

  config.before(:suite) do
    Paysimple.api_user = ENV['API_USER']
    Paysimple.api_key = ENV['API_KEY']
    Paysimple.mode = :test
  end

  VCR.configure do |c|
    c.cassette_library_dir = 'spec/cassettes'
    c.default_cassette_options = { match_requests_on: [:host, :path] }
    c.hook_into :webmock
    c.ignore_localhost = false
  end
end
