# frozen_string_literal: true

require 'time'
require 'digest'
require 'base64'

require 'rest-client'
require 'json'

# Common
require 'paysimple/version'
require 'paysimple/endpoint'
require 'paysimple/util'
require 'paysimple/config'

# Enumerations
require 'paysimple/enumerations/issuer'
require 'paysimple/enumerations/payment_type'
require 'paysimple/enumerations/payment_sub_type'
require 'paysimple/enumerations/schedule_status'
require 'paysimple/enumerations/payment_status'
require 'paysimple/enumerations/execution_frequency_type'

# Resources
require 'paysimple/resources/customer'
require 'paysimple/resources/credit_card'
require 'paysimple/resources/ach'
require 'paysimple/resources/payment'
require 'paysimple/resources/recurring_payment'
require 'paysimple/resources/payment_plan'
require 'paysimple/resources/checkout_token'
require 'paysimple/resources/webhook_subscription'

# Errors
require 'paysimple/errors/paysimple_error'
require 'paysimple/errors/api_connection_error'
require 'paysimple/errors/api_error'
require 'paysimple/errors/invalid_request_error'
require 'paysimple/errors/authentication_error'
require 'paysimple/errors/not_found_error'
require 'paysimple/errors/parse_error'

# Paysimple
module Paysimple
  @ssl_version = Config::DEFAULT_SSL_VERSION

  class << self
    attr_accessor :api_key, :api_user, :mode, :ssl_version

    def request(method, url, params = {})
      raise AuthenticationError, 'API key is not provided.' unless @api_key
      raise AuthenticationError, 'API user is not provided.' unless @api_user

      api_version = params.delete(:api_version)

      url = if api_version == 'v5'
        api_v5_url(url)
      else
        api_url(url)
      end

      case method
      when :get, :head
        url += "#{URI.parse(url).query ? '&' : '?'}#{uri_encode(params)}" if params && params.any?
        payload = nil
      when :delete
        payload = nil
      else
        if api_version == 'v5'
          payload = params.to_json
        else
          payload = Util.camelize_and_symbolize_keys(params).to_json
        end
      end

      request_opts = {
        headers: request_headers(api_version),
        method: method,
        open_timeout: 30,
        payload: payload,
        url: url,
        timeout: 80,
        ssl_version: ssl_version
      }

      begin
        response = execute_request(request_opts)
      rescue SocketError => exeception
        handle_restclient_error(exeception)
      rescue RestClient::ExceptionWithResponse => exeception
        if (rcode = exeception.http_code) && (rbody = exeception.http_body)
          handle_api_error(rcode, rbody, api_version)
        else
          handle_restclient_error(exeception)
        end
      rescue RestClient::Exception, Errno::ECONNREFUSED => exception
        handle_restclient_error(exception)
      end

      parse(response)
    end

    def api_endpoint
      @mode ||= :live

      if @mode.to_sym == :live
        Paysimple::Endpoint::PRODUCTION
      else
        Paysimple::Endpoint::SANDBOX
      end
    end

    def api_url(url = '')
      api_endpoint + '/v4' + url
    end

    def api_v5_url(url = '')
      api_endpoint + '/ps' + url
    end

    def execute_request(opts)
      RestClient::Request.execute(opts)
    end

    def request_headers(api_version)
      auth_header = api_version == 'v5' ? basic_authorization_header : authorization_header
      {
        authorization: auth_header,
        content_type: :json,
        accept: :json
      }
    end

    def uri_encode(params)
      Util.flatten_params(params).map { |k, v| "#{k}=#{Util.url_encode(v)}" }.join('&')
    end

    def parse(response)
      if response.body.empty?
        {}
      else
        response = JSON.parse(response.body)
        Util.underscore_and_symbolize_names(response)
      end
    rescue JSON::ParserError
      raise general_api_error(response.code, response.body)
    end

    def general_api_error(rcode, rbody)
      APIError.new("Invalid response object from API: #{rbody.inspect} " \
                       "(HTTP response code was #{rcode})", rcode, rbody)
    end

    def basic_authorization_header
      "basic #{@api_user}:#{@api_key}"
    end

    def authorization_header
      utc_timestamp = Time.now.getutc.iso8601.encode(Encoding::UTF_8)
      secret_key = @api_key.encode(Encoding::UTF_8)
      hash = OpenSSL::HMAC.digest(OpenSSL::Digest.new('sha256'), secret_key, utc_timestamp)
      signature = Base64.encode64(hash)

      "PSSERVER accessid=#{@api_user}; timestamp=#{utc_timestamp}; signature=#{signature}"
    end

    def handle_api_error(rcode, rbody, api_version)
      begin
        error_obj = rbody.empty? ? {} : Util.underscore_and_symbolize_names(JSON.parse(rbody))
      rescue JSON::ParserError
        raise general_api_error(rcode, rbody)
      end

      case rcode
      when 400, 404
        if api_version == 'v5'
          errors = error_obj[:error_messages].collect { |e| e[:message] }
        else
          errors = error_obj[:meta][:errors][:error_messages].collect { |e| e[:message] }
        end

        raise InvalidRequestError.new(errors, rcode, rbody, error_obj) if rcode == 400
        raise NotFoundError.new(errors, rcode, rbody, error_obj) if rcode == 404
      when 401
        error = 'Error getting an API token. Check your authentication credentials and resubmit.'
        raise AuthenticationError.new(error, rcode, rbody, error_obj)
      else
        error = 'There was an error processing the request.'
        raise APIError.new(error, rcode, rbody, error_obj)
      end
    end

    def handle_restclient_error(exeception, _api_base_url = nil)
      connection_message = 'Please check your internet connection and try again.'

      message = case exeception
                when RestClient::RequestTimeout
                  "Could not connect to Paysimple (#{api_endpoint}). #{connection_message}"
                when RestClient::ServerBrokeConnection
                  "The connection to the server (#{api_endpoint}) broke before the " \
                  "request completed. #{connection_message}"
                when SocketError
                  'Unexpected error communicating when trying to connect to Paysimple. ' \
                  'You may be seeing this message because your DNS is not working. '
                else
                  'Unexpected error communicating with Paysimple. '
                end

      raise APIConnectionError, message + "\n\n(Network error: #{exception.message})"
    end
  end
end
