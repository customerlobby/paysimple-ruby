# frozen_string_literal: true

module Paysimple
  class Issuer
    VISA = 12
    MASTER_CARD = 13
    AMERICAN_EXPRESS = 14
    DISCOVER = 15
    MASTERCARD = 13
    AMERICANEXPRESS = 14
    AMEX = 14
  end
end
