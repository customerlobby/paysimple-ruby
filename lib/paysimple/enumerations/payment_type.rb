# frozen_string_literal: true

module Paysimple
  class PaymentType
    CREDIT_CARD = 1
    ACH = 2
  end
end
