# frozen_string_literal: true

module Paysimple
  class APIConnectionError < PaysimpleError
  end
end
