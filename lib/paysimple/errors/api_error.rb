# frozen_string_literal: true

module Paysimple
  class APIError < PaysimpleError
  end
end
