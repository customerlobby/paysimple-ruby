# frozen_string_literal: true

module Paysimple
  class InvalidRequestError < PaysimpleError
  end
end
