# frozen_string_literal: true

module Paysimple
  class NotFoundError < PaysimpleError
  end
end
