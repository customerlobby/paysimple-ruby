# frozen_string_literal: true

module Paysimple
  class AuthenticationError < PaysimpleError
  end
end
