# frozen_string_literal: true

module Paysimple
  class ParseError < PaysimpleError
  end
end
