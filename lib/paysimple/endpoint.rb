# frozen_string_literal: true

module Paysimple
  class Endpoint
    PRODUCTION = 'https://api.paysimple.com'.freeze
    SANDBOX = 'https://sandbox-api.paysimple.com'.freeze
  end
end
