# frozen_string_literal: true

module Paysimple
  class Config
    DEFAULT_SSL_VERSION = 'TLSv1_2'.freeze
  end
end
