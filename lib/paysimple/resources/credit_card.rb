# frozen_string_literal: true

module Paysimple
  class CreditCard
    def self.create(opts)
      Paysimple.request(:post, url, opts)
    end

    def self.update(opts)
      Paysimple.request(:put, url, opts)
    end

    def self.delete(id)
      Paysimple.request(:delete, "#{url}/#{id}")
    end

    def self.get(id)
      Paysimple.request(:get, "#{url}/#{id}")
    end

    def self.fetch(id)
      Paysimple.request(:get, "/customer/#{id}/defaultcreditcard")
    end

    def self.url
      '/account/creditcard'
    end
  end
end
