# frozen_string_literal: true

module Paysimple
  class WebhookSubscription
    API_VERSION = 'v5'

    class << self
      def create(options)
        options[:api_version] = API_VERSION
        Paysimple.request(:post, path, options)
      end

      def update(id, options)
        options[:api_version] = API_VERSION
        Paysimple.request(:put, "#{path}/#{id}", options)
      end

      def get(id)
        Paysimple.request(:get, "#{path}/#{id}", {api_version: API_VERSION})
      end

      def delete(id)
        Paysimple.request(:delete, "#{path}/#{id}", {api_version: API_VERSION})
      end

      protected

      def path
        '/webhook/subscription'
      end
    end
  end
end
