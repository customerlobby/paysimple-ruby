# frozen_string_literal: true

module Paysimple
  class CheckoutToken
    def self.create
      Paysimple.request(:post, url)
    end

    def self.url
      '/checkouttoken'
    end
  end
end
