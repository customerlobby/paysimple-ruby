# frozen_string_literal: true

module Paysimple
  class RecurringPayment
    def self.create(opts)
      Paysimple.request(:post, url, opts)
    end

    def self.get(id)
      Paysimple.request(:get, "#{url}/#{id}")
    end

    def self.update(id, opts)
      Paysimple.request(:put, "#{url}/#{id}", opts)
    end

    def self.suspend(id)
      Paysimple.request(:put, "#{url}/#{id}/suspend")
    end

    def self.resume(id)
      Paysimple.request(:put, "#{url}/#{id}/resume")
    end

    def self.delete(id)
      Paysimple.request(:delete, "#{url}/#{id}")
    end

    def self.payments(id, opts)
      Paysimple.request(:get, "#{url}/#{id}/payments", opts)
    end

    def self.find(opts)
      Paysimple.request(:get, url, opts)
    end

    def self.pause(id, enddate)
      Paysimple.request(:put, "#{url}/#{id}/pause?enddate=#{enddate}")
    end

    def self.url
      '/recurringpayment'
    end
  end
end
